Feature: Add Emails
  In order to have people vote on the proposal
  As a person who created a proposal that I would like to have others vote on
  I want to be able to add their emails

  Scenario: See the email add box
    Given
    When I have created a proposal
    Then I should be on the proposal confirmation page
    Then I should see "Send this proposal to: "
    And I should see a field to enter email
