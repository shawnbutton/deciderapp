Feature: View My Proposals
  In order to check be able to manage all of the proposals I have made
  As a person who has created multiple proposal
  I want to be see a page with that lists all of my proposals


  Scenario: View a list of all of my proposals
    Given There is an existing user with multiple proposals
    Given I am logged in to the site
    And I go to the home page
    Then I should see "My Proposals"
    When I navigate to the my proposal page
    Then I should see "List Of My Proposals"
    And My proposals should be listed


  Scenario: View a list of proposals but the user has none
    Given There is an existing user
    Given I am logged in to the site
    When I go to the my proposal page
    Then I should see "You have not created any proposals yet"

  Scenario: After viewing a list of proposals should be able to click on one to see it
    Given There is an existing user with multiple proposals
    Given I am logged in to the site
    When I go to the my proposal page
    And I choose a proposal
    Then I should be on the proposal confirmation page
    And I should see the details of my proposal