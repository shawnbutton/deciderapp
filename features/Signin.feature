Feature: Signin to site
  In order to have the benefits that come with being signed in
  As a person who wants to have multiple interactions
  I want to be able to sign in to the site

  Scenario: Able to see sign in page
    Given I go to the home page
    When I navigate to the signin page
    Then I should be on the signin page
    And I should see the correct elements of the signin page

  Scenario: Able to sign in
    Given There is an existing user
    And I am on the signin page
    When I enter my existing user name
    And I enter my password
    And I submit my signin information
    Then I should be on the proposal creation page
    And I should be signed in





