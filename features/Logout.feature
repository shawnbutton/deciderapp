Feature: SignOut of site
  In order to have not leave my browser logged in to the site
  As a person who has a a user profile
  I want to be able to sign out of the site

  Scenario: Able to sign out
    Given There is an existing user
    And I am logged in to the site
    When I go to the home page
    And I logout
    Then I should be logged out





