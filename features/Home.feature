Feature: Home
  In order to be welcomed to the site
  As a person who wants to interact with Let's Decide
  I want to be see a home page that let's me navigate to the different features I need

  Scenario: See home page
    Given
    When I go to the home page
    Then I should see "Welcome to Let's Decide"

  Scenario: Navigate to Create Proposal page
    Given
    When I go to the home page
    And I navigate to the proposal creation page
    Then I should be on the proposal creation page



