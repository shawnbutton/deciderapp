Feature: Vote
  In order to vote on a proposal
  As a person who has received an invite to vote
  I want to be able to cast my vote for a proposal

  Scenario: See Vote Page
    Given There is an existing proposal
    When I go to the vote page
    Then I should see the proposal title
    And I should see "Yes"
    And I should see "No"
    And I should see "Maybe"


  Scenario: Can Cast Yes Vote
    Given There is an existing proposal
    When I go to the vote page
    And I choose Yes
    And I submit
    Then I should see "Vote registered"
    And I should see "You choose: Yes"
    And the number of votes should be yes:"1", no:"0", maybe:"0"

  Scenario: Can Cast No Vote
    Given There is an existing proposal
    When I go to the vote page
    And I choose No
    And I submit
    Then I should see "Vote registered"
    And I should see "You choose: No"
    And the number of votes should be yes:"0", no:"1", maybe:"0"

  Scenario: Can Cast Maybe Vote
    Given There is an existing proposal
    When I go to the vote page
    And I choose Maybe
    And I submit
    Then I should see "Vote registered"
    And I should see "You choose: Maybe"
    And the number of votes should be yes:"0", no:"0", maybe:"1"


  Scenario: Can add name, email address and vote is recorded
    Given There is an existing proposal
    When I go to the vote page
    And I choose Yes
    And I enter my name
    And I enter my email address
    And I submit
    Then there should be a YES vote registered with the correct name and email


Scenario: If logged in and voting then name and email are pre-filled
    Given There is an existing user
    And I am logged in to the site
    And There is an existing proposal
    When I go to the vote page
    Then My details are pre-filled

