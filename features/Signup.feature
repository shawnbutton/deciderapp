Feature: Signup (Create User Profile)
  In order to have the benefits that come with being registered
  As a person who wants to have multiple interactions
  I want to be able to register and create a profile (signup)

  Scenario: Able to see signup page
    Given I go to the home page
    When I navigate to the signup page
    Then I should be on the signup page
    And I should see the correct elements of the signup page

  Scenario: Able to sign up
    Given I go to the signup page
    When I enter a user name
    When I enter a password "somepassword"
    And I enter a password confirmation "somepassword"
    And I submit the user info
    Then I should be on the proposal creation page
    And I should see "Welcome! You have signed up successfully."
    And A user account should be created

  Scenario: Try to sign up but email not given
    Given I go to the signup page
    When I enter a password "somepassword"
    And I enter a password confirmation "somepassword"
    And I submit the user info
    Then I should see "Email can't be blank"

  Scenario: Email in wrong format
    Given I go to the signup page
    When I enter a user name
    When I enter a password "123"
    And I submit the user info
    Then I should see "Password is too short"


  Scenario: Try to sign up but password not given
    Given I go to the signup page
    When I enter a user name
    And I enter a password confirmation "somepassword"
    And I submit the user info
    Then I should see "Password can't be blank"

  Scenario: Try to sign up but password confirmation not given
    Given I go to the signup page
    When I enter a user name
    When I enter a password "somepassword"
    And I submit the user info
    Then I should see "Password confirmation doesn't match Password"

  Scenario: Try to sign up but Incorrect password confirmation
    Given I go to the signup page
    When I enter a password "somepassword"
    And I enter a password confirmation "otherpassword"
    And I submit the user info
    Then I should see "Password confirmation doesn't match Password"

  Scenario: Try to sign up but email is already taken
    Given There is an existing user
    And I go to the signup page
    When I enter a user name
    And I enter a password "somepassword"
    And I enter a password confirmation "somepassword"
    And I submit the user info
    Then I should see "Email has already been taken"




