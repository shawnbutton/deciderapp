Feature: Propose
  In order to make a proposal
  As a person who has a proposal that I would like to have others vote on
  I want to be able to create a proposal

  Scenario: See Proposal Page
    Given
    When I go to the proposal page
    Then I should see "Make a proposal"
    And I should see the correct elements of the proposal page

  Scenario: Enter a Proposal and get a URL
    Given
    When I go to the proposal page
    And enter the fields of the proposal
    And submit the proposal
    Then I should be on the proposal confirmation page
    And I should see "Your Proposal"
    And I should see "Send them this link:"
    And I should see the details of my proposal

  Scenario: Proposal without title gives an error
    Given
    When I go to the proposal page
    And enter the fields of the proposal
    And do not enter the title
    And submit the proposal
    Then I should see "Title can't be blank"

  Scenario: Proposal without details gives an error
    Given
    When I go to the proposal page
    And enter the fields of the proposal
    And do not enter details
    And submit the proposal
    Then I should see "Details can't be blank"

  Scenario: Create proposal and go to voting page
    Given
    When I go to the proposal page
    And enter the fields of the proposal
    And submit the proposal
    And navigate to the vote url
    Then the voting page should be shown

  Scenario: Create proposal and go to stats page
    Given
    When I go to the proposal page
    And enter the fields of the proposal
    And submit the proposal
    And navigate to the stats url
    Then the stats page should be shown

  Scenario: Proposal without email gives an error
    Given
    When I go to the proposal page
    And enter the fields of the proposal
    And do not enter email
    And submit the proposal
    Then I should see "Email can't be blank"

  Scenario: Proposal with invalid email gives an error
    Given
    When I go to the proposal page
    And enter the fields of the proposal
    And enter an invalid email address
    And submit the proposal
    Then I should see "Email is invalid"

  Scenario: When a new proposal is created an email is sent
    Given We know how many emails have been sent so far
    When I go to the proposal page
    And enter the fields of the proposal
    And submit the proposal
    Then There should be one more email sent
    And an email should have been sent

  Scenario: When a new proposal is created and I am logged in then my email address is pre-filled
    Given There is an existing user
    And I am logged in to the site
    When I go to the proposal page
    Then I should be signed in
    Then The email field is pre-filled with my email address

  Scenario: When a new proposal is created and I am logged in then the proposal is saved to my user
    Given There is an existing user
    And I am logged in to the site
    When I go to the proposal page
    And enter the fields of the proposal
    And submit the proposal
    Then My user should have a proposal associated to it





