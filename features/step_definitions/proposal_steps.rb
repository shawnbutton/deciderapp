When(/^I go to the proposal page$/) do
  @proposal_page = ProposalCreationPage.new
  @proposal_page.load
end

And(/^enter the fields of the proposal$/) do
  @proposal_page.fill_in_fields_with_default_values
end

And(/^submit the proposal$/) do
  @proposal_page.submit
end

And(/^do not enter the title$/) do
  @proposal_page.proposal_title.set ''
end

And(/^do not enter details$/) do
  @proposal_page.proposal_details.set ''
end

And(/^navigate to the vote url$/) do
  @proposal_page.vote_url.click
end

And(/^navigate to the stats url$/) do
  @proposal_page.stats_url.click
end

And(/^I should see the correct elements of the proposal page$/) do
  expect(@proposal_page).to have_text 'I propose that:'
  expect(@proposal_page).to have_proposal_title

  expect(@proposal_page).to have_text 'Details'
  expect(@proposal_page).to have_proposal_details

  expect(@proposal_page).to have_text 'Email'
  expect(@proposal_page).to have_proposal_email

  expect(@proposal_page).to have_text 'Name'
  expect(@proposal_page).to have_proposal_name

end

And(/^do not enter email$/) do
  @proposal_page.proposal_email.set ''
end

And(/^enter an invalid email address$/) do
  @proposal_page.proposal_email.set 'crappyemailaddress'
end

Then(/^an email should have been sent$/) do
  proposal_email = ActionMailer::Base.deliveries.last

  proposal_attributes = FactoryGirl.attributes_for(:proposal)

  expect(proposal_email.subject).to eq('Your New Proposal Has Been Created:' + proposal_attributes[:title])
  expect(proposal_email.to).to eq([proposal_attributes[:email]])
  #todo verify email body
  #expect(proposal_email.body).to include('proposal')

end

Given(/^We know how many emails have been sent so far$/) do
    @number_of_emails_previously_delivered = ActionMailer::Base.deliveries.size
end


Then(/^There should be one more email sent$/) do
  number_of_emails_delivered = ActionMailer::Base.deliveries.size
  expect(number_of_emails_delivered).to eq(@number_of_emails_previously_delivered + 1)
end

Then(/^My user should have a proposal associated to it$/) do
  proposal = Proposal.find_by_email('shawnbutton@gmail.com')
  expect(proposal.user)
end

Then(/^The email field is pre\-filled with my email address$/) do
  expect(@proposal_page.proposal_email.value).to eq('shawnbutton@gmail.com')
end

