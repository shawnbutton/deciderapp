class ProposalConfirmationPage < SitePrism::Page

  set_url "/proposals"
  set_url_matcher /proposals/

  element :invitee_email, '#invitee_email'
  element :invitee_submit, '#invitee_submit'


end