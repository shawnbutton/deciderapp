class VotePage < SitePrism::Page
  set_url "/vote/show{/proposal_id}"
  set_url_matcher /vote\/show/

  element :choice_Yes, '#choice_Yes'
  element :choice_No, '#choice_No'
  element :choice_Maybe, '#choice_Maybe'

  element :vote_email, '#vote_email'
  element :vote_name, '#vote_name'


  element :vote_submit, "#vote_submit"


  def submit
    vote_submit.click
  end

  def enter_name(name)
    vote_name.set name
  end

  def enter_email(email)
    vote_email.set email
  end

  def email
    vote_email.text
  end

  def name
    vote_name.text
  end

end