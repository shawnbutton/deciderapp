class SigninPage < SitePrism::Page
  set_url '/users/sign_in'
  set_url_matcher /users\/sign_in/

  element :user_email, '#user_email'
  element :user_password, '#user_password'

  element :user_remember_me, "#user_remember_me"

  element :user_submit, "input[name='commit']"

  def submit
    user_submit.click
  end


end