class SignupPage < SitePrism::Page
  set_url '/users/sign_up'
  set_url_matcher /users\/sign_up/

  element :user_email, '#user_email'
  element :user_password, '#user_password'
  element :user_password_confirmation, '#user_password_confirmation'

  element :user_submit, "input[name='commit']"

  def submit
    user_submit.click
  end


end