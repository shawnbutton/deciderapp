class HeaderPage < SitePrism::Page

  element :proposal_link, '#proposal_create'
  element :signup_link, '#signup'
  element :signin_link, '#signin'
  element :logout_link, '#logout'
  element :my_proposals_link, '#view__my_proposals'

  def is_signed_in?
    has_content? "Logged in as"
  end

end