class StatsPage < SitePrism::Page
  set_url "/stats/view{/proposal_id}"

  set_url_matcher /stats\/view/

end