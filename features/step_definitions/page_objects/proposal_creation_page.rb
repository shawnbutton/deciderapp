class ProposalCreationPage < SitePrism::Page
  set_url "/proposals/new"
  set_url_matcher /proposals\/new/

  element :proposal_title, '#proposal_title'
  element :proposal_details, '#proposal_details'
  element :proposal_email, '#proposal_email'
  element :proposal_name, '#proposal_name'

  element :proposal_submit, "#create_proposal"

  element :vote_url, "#vote_url"
  element :stats_url, "#stats_url"

  def fill_in_fields_with_default_values
    proposal_attributes = FactoryGirl.attributes_for(:proposal)

    proposal_title.set proposal_attributes[:title]
    proposal_details.set proposal_attributes[:details]
    proposal_email.set proposal_attributes[:email]
  end

  def submit
    proposal_submit.click
  end

  def create_proposal
    load
    fill_in_fields_with_default_values
    submit
    ProposalConfirmationPage.new
  end

end