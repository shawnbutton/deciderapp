class MyProposalsPage < SitePrism::Page
  set_url "/my_proposals/view"

  element :proposal_title, '#proposal_title'
  element :proposal_details, '#proposal_details'
  element :proposal_email, '#proposal_email'
  element :proposal_name, '#proposal_name'

  element :first_proposal_link, '#proposal_link_1'

  def click_first_proposal
    first_proposal_link.click
  end


end