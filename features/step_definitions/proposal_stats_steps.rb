TOTAL_YES = 44
TOTAL_MAYBE = 55
TOTAL_NO = 66


Given(/^There is a proposal with votes$/) do

  @proposal = FactoryGirl.create(:proposal, total_yes: TOTAL_YES, total_no: TOTAL_NO, total_maybe: TOTAL_MAYBE)

  voter = @proposal.voters.create
  voter.email = 'somevoter@gmail.com'
  voter.name = 'some voter'
  voter.vote = 'yes'
  voter.save

  voter = @proposal.voters.create
  voter.email = 'someSecondVoter@gmail.com'
  voter.name = 'some second voter'
  voter.vote = 'no'
  voter.save

end


When(/^I go to the proposal stats page$/) do
  @stats_page = StatsPage.new
  @stats_page.load(proposal_id: @proposal.proposal_id)
end

And(/^the displayed number of votes should be correct$/) do
  expect(@stats_page).to have_text 'Yes: ' + TOTAL_YES.to_s
  expect(@stats_page).to have_text 'Maybe: ' +TOTAL_MAYBE.to_s
  expect(@stats_page).to have_text 'No: ' + TOTAL_NO.to_s

end

And(/^the stats page should be shown$/) do
  @stats_page = StatsPage.new
  expect @stats_page.displayed?
end

And(/^I should see details on who voted$/) do

  expect(@stats_page).to have_text 'somevoter@gmail.com'
  expect(@stats_page).to have_text 'some voter'

end


Then(/^Each vote is displayed$/) do
  expect(@stats_page).to have_text 'somevoter@gmail.com'
  expect(@stats_page).to have_text 'some voter'
  expect(@stats_page).to have_text 'yes'

  expect(@stats_page).to have_text 'someSecondVoter@gmail.com'
  expect(@stats_page).to have_text 'some second voter'
  expect(@stats_page).to have_text 'no'

end