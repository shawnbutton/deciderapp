
Given(/^I go to the vote page$/) do
  @vote_page = VotePage.new
  @vote_page.load(proposal_id: @proposal.proposal_id)
end

And(/^I choose Yes$/) do
  @vote_page.choice_Yes.set true
end

And(/^I choose No$/) do
  @vote_page.choice_No.set true
end

And(/^I choose Maybe$/) do
  @vote_page.choice_Maybe.set true
end

Then(/^I should see "([^"]*)"$/) do |arg|
  expect(page).to have_text arg
end


And(/^I submit$/) do
  @vote_page.submit
end

Given(/^There is an existing proposal$/) do
  @proposal = FactoryGirl.create(:proposal)
end


Then(/^I should see the proposal title$/) do
  expect(@vote_page).to have_text @proposal.title
end

Then(/^the voting page should be shown/) do
  vote_page = VotePage.new
  expect vote_page.displayed?
end


And(/^the number of votes should be yes:"([^"]*)", no:"([^"]*)", maybe:"([^"]*)"$/) do |yes, no, maybe|
  expect(@vote_page).to have_text "Yes: #{yes} vote(s)"
  expect(@vote_page).to have_text "No: #{no} vote(s)"
  expect(@vote_page).to have_text "Maybe: #{maybe} vote(s)"
end

And(/^I enter my name$/) do
  @voter_name = 'Shawn Button'
  @vote_page.enter_name(@voter_name)
end


And(/^I enter my email address$/) do
  @voter_email = 'shawnbutton@gmail.com'
  @vote_page.enter_email(@voter_email)
end

Then(/^there should be a YES vote registered with the correct name and email$/) do
  voters = @proposal.voters
  expect(voters.size).to eq(1)

  voter = voters[0]
  expect(voter.email).to eq(@voter_email)
  expect(voter.vote).to eq('yes')

end

Then(/^My details are pre\-filled$/) do
  user_attributes = FactoryGirl.attributes_for(:user)

  expect(@vote_page.email).to eq(user_attributes[:email])
  # expect(@vote_page.name).to eq(user_attributes[:name])
end