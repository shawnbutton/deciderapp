Then(/^I should be on the signin page$/) do
  @signin_page = SigninPage.new
  expect @signin_page.displayed?
end

And(/^I should see the correct elements of the signin page$/) do
  expect(@signin_page).to have_text 'Sign in'

  expect(@signin_page).to have_text 'Email'
  expect(@signin_page).to have_user_email

  expect(@signin_page).to have_text 'Password'
  expect(@signin_page).to have_user_password

  expect(@signin_page).to have_text 'Remember me'
  expect(@signin_page).to have_user_remember_me

  expect(@signin_page).to have_user_submit

end

When(/^I enter my existing user name$/) do
  @signin_page.user_email.set @user.email
end


And(/^I am on the signin page$/) do
  @signin_page = SigninPage.new
  @signin_page.load
end

And(/^I enter my password$/) do
  @signin_page.user_password.set @user.password
end

And(/^I submit my signin information$/) do
  @signin_page.submit
end

And(/^I should be signed in$/) do
  # check the contents of the page
  header_page = HeaderPage.new
  expect(header_page).to have_content 'Logged in as'

  # check devise
  expect @user.sign_in_count == 1

end