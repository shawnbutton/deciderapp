When(/^I navigate to the my proposal page$/) do
  header_page = HeaderPage.new
  header_page.my_proposals_link.click

  @my_proposals_page = MyProposalsPage.new
end

When(/^I go to the my proposal page$/) do

  @my_proposals_page = MyProposalsPage.new
  @my_proposals_page.load

end


And(/^My proposals should be listed$/) do
# todo validate more of the proposal listing
  proposal_attributes = FactoryGirl.attributes_for(:proposal)

  expect(@my_proposals_page).to have_text proposal_attributes[:title]
end


And(/^I choose a proposal$/) do
  @my_proposals_page.click_first_proposal
end
