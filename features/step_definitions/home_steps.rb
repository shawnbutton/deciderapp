# the following are needed to include so that we can use devise (which uses warden behind the scenes) to log in
include Warden::Test::Helpers
Warden.test_mode!

When(/^I go to the home page$/) do
  @home_page = HomePage.new
  @home_page.load
end

And(/^I navigate to the proposal creation page$/) do
  @home_page.proposal_link.click
end

Then(/^I should be on the proposal creation page$/) do
  @proposal_creation_page = ProposalCreationPage.new
  expect @proposal_creation_page.displayed?
end

When(/^I navigate to the signup page$/) do
  @home_page.signup_link.click
end

When(/^I navigate to the signin page$/) do
  @home_page.signin_link.click
end


Given(/^I am logged in to the site$/) do
  login_as(@user, :scope => :user)
end

And(/^I logout$/) do
  header_page = HeaderPage.new
  header_page.logout_link.click
end

Then(/^I should be logged out$/) do
  expect @user.sign_in_count == 0
  header_page = HeaderPage.new
  expect(header_page.is_signed_in?).to be false

end