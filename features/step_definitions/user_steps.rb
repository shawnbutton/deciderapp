Given(/^There is an existing user with multiple proposals$/) do
  @user = FactoryGirl.create(:user)
  # make 5 proposals
  FactoryGirl.create(:proposal, user: @user)
  FactoryGirl.create(:proposal, user: @user, title: 'title2')
  FactoryGirl.create(:proposal, user: @user, title: 'title3')
  FactoryGirl.create(:proposal, user: @user, title: 'title4')
  FactoryGirl.create(:proposal, user: @user, title: 'title5')
end

Given(/^There is an existing user$/) do
  @user = FactoryGirl.create(:user)
end