
And(/^I should see the correct elements of the signup page$/) do
  expect(@signup_page).to have_text 'Sign up'

  expect(@signup_page).to have_text 'Email'
  expect(@signup_page).to have_user_email

  expect(@signup_page).to have_text 'Password'
  expect(@signup_page).to have_user_password

  expect(@signup_page).to have_text 'Password confirmation'
  expect(@signup_page).to have_user_password_confirmation

  expect(@signup_page).to have_user_submit

end

def instantiate_signup_page
  @signup_page = SignupPage.new
end

Then(/^I should be on the signup page$/) do
  instantiate_signup_page

  expect @signup_page.displayed?

end

When(/^I go to the signup page$/) do
  instantiate_signup_page

  @signup_page.load
end

And(/^I submit the user info$/) do
  @signup_page.submit
end

And(/^A user account should be created$/) do
  user = User.find_by(email: 'shawnbutton@gmail.com')
  expect !user.nil?
end

When(/^I enter a user name$/) do
  @signup_page.user_email.set 'shawnbutton@gmail.com'
end

When(/^I enter a password "([^"]*)"$/) do |arg|
  @signup_page.user_password.set arg
end

And(/^I enter a password confirmation "([^"]*)"$/) do |arg|
  @signup_page.user_password_confirmation.set arg
end
