Then(/^I should be on the proposal confirmation page$/) do
  @prop_confirm = ProposalConfirmationPage.new
  expect @prop_confirm.displayed?
end


And(/^I should see a field to enter email$/) do
  expect(@prop_confirm).to have_invitee_email
end


And(/^I should see the details of my proposal$/) do
  expect(@prop_confirm).to have_text 'An email has been sent to you with these details.'

  proposal_attributes = FactoryGirl.attributes_for(:proposal)

  expect(@prop_confirm).to have_text proposal_attributes[:title]
  expect(@prop_confirm).to have_text proposal_attributes[:details]
  expect(@prop_confirm).to have_text proposal_attributes[:email]

end
