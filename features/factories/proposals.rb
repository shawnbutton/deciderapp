FactoryGirl.define do
  factory :proposal do
    title "some proposal title"
    details "some details for the proposal"
    email "shawnbutton@gmail.com"
  end
end