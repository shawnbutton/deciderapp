Feature: Proposal Stats
  In order to check on the results of a proposal
  As a person who has created a proposal
  I want to be see a page with details on the proposal votes


  Scenario: View the proposal stats
    Given There is a proposal with votes
    When I go to the proposal stats page
    Then I should see "Stats for proposal"
    And the stats page should be shown
    And the displayed number of votes should be correct
    And I should see details on who voted

  Scenario: View a list of voters and their votes
    Given There is a proposal with votes
    When I go to the proposal stats page
    Then I should see "People who voted:"
    And Each vote is displayed


