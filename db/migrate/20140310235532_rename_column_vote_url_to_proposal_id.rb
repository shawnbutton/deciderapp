class RenameColumnVoteUrlToProposalId < ActiveRecord::Migration
  def change
    change_table :proposals do |t|
      t.rename :vote_url, :proposal_id
    end
  end
end
