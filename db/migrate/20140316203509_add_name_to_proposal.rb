class AddNameToProposal < ActiveRecord::Migration
  def change
    add_column :proposals, :name, :string
  end
end
