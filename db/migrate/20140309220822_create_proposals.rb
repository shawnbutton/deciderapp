class CreateProposals < ActiveRecord::Migration
  def change
    create_table :proposals do |t|
      t.string :title
      t.text :details
      t.text :vote_url
      t.integer :total_yes
      t.integer :total_maybe
      t.integer :total_no

      t.timestamps
    end
  end
end
