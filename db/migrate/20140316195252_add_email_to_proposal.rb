class AddEmailToProposal < ActiveRecord::Migration
  def change
    add_column :proposals, :email, :string
  end
end
