class CreateVoters < ActiveRecord::Migration
  def change
    create_table :voters do |t|
      t.string :proposal_id
      t.string :name
      t.string :email
      t.boolean :voted
      t.string :vote

      t.timestamps
    end
  end
end
