json.array!(@proposals) do |proposal|
  json.extract! proposal, :id, :title, :details, :email, :name, :proposal_id, :total_yes, :total_maybe, :total_no
  json.url proposal_url(proposal, format: :json)
end
