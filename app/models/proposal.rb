class Proposal < ActiveRecord::Base
  belongs_to :user

  has_many :voters

  validates :title, :details, :email, presence: true
  validates :proposal_id, uniqueness: true

  validates_format_of :email, :with => /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/


  after_initialize :init

  def init
    if self.new_record?
    # create a random string to identify this record, ONLY if brand new record
      self.proposal_id = create_random_id
      self.total_yes = 0
      self.total_maybe = 0
      self.total_no = 0
    end
  end

  def create_random_id

    all_letters = [('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten

    sixteen_characters = (0...16).map { all_letters[rand(all_letters.length)] }.join

    sixteen_characters
  end

  def voting_url
    "/vote/show/" + proposal_id
  end

  def stats_url
    "/stats/view/" + proposal_id
  end

  def proposal_url
    "/proposals/" + proposal_id
  end



end
