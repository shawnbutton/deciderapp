class ProposalsController < ApplicationController
  #before_action :set_proposal, only: [:show, :edit, :update, :destroy]

  # GET /proposals
  # GET /proposals.json
  #def index
  #  @proposals = Proposal.all
  #end

  # GET /proposals/:proposal_id
  # GET /proposals/1.json
  def show
    proposal_id = params[:proposal_id]
    @proposal = Proposal.find_by(proposal_id: proposal_id)
  end


  # GET /proposals/new
  def new
    @proposal = Proposal.new
  end

  # GET /proposals/1/edit
  def edit
    proposal_id = params[:proposal_id]
    @proposal = Proposal.find_by(proposal_id: proposal_id)
  end

  # POST /proposals
  # POST /proposals.json
  def create

    if user_signed_in?
      @proposal = current_user.proposals.create(proposal_params)
    else
      @proposal = Proposal.new(proposal_params)
    end

    respond_to do |format|
      if @proposal.save

        # todo is this next used?
        @email = ''

        # send an email confirming the proposal was created
        ProposalMailer.proposal_email(@proposal).deliver

        format.html { redirect_to controller: 'proposals', action: 'show', proposal_id: @proposal.proposal_id }
        format.json { redirect_to action: 'show', status: :created, location: @proposal }
      else
        format.html { render action: 'new' }
        format.json { render json: @proposal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /proposals/1
  # PATCH/PUT /proposals/1.json
  def update
    respond_to do |format|
      if @proposal.update(proposal_params)
        format.html { redirect_to @proposal, notice: 'Proposal was successfully updated.' }
        format.json { render action: 'show', status: :ok, location: @proposal }
      else
        format.html { render action: 'edit' }
        format.json { render json: @proposal.errors, status: :unprocessable_entity }
      end
    end
  end

  def confirm

  end


  # DELETE /proposals/1
  # DELETE /proposals/1.json
  def destroy
    @proposal.destroy
    respond_to do |format|
      format.html { redirect_to proposals_url }
      format.json { head :no_content }
    end
  end


  def email

    respond_to do |format|
      permitted_params = params.permit(:invitee_email, :id)

      invite_email = permitted_params[:invitee_email]
      id = permitted_params[:id]

      @proposal = Proposal.find(id)
      @voter = @proposal.voters.create
      @voter.email = invite_email
      if @voter.save

        # send an invite email
        AddVoterMailer.add_voter_email(@voter, @proposal).deliver

        format.js
        format.json
      end

    end

  end

  private
  # Use callbacks to share common setup or constraints between actions.
  #def set_proposal
    #@proposal = Proposal.find(params[:id])
  #end

  # Never trust parameters from the scary internet, only allow the white list through.
  def proposal_params
    params.require(:proposal).permit(:title, :details, :email, :name, :proposal_id, :total_yes, :total_maybe, :total_no)
  end
end
