class StatsController < ApplicationController

  def view
    proposal_id = params[:proposal_id]
    @proposal = Proposal.find_by(proposal_id: proposal_id)
    @voters = @proposal.voters
  end

end
