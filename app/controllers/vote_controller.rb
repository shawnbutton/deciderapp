class VoteController < ApplicationController
  def vote
    proposal_id = params[:proposal_id]
    @proposal = Proposal.find_by(proposal_id: proposal_id)

    @vote = params[:choice]

    if @vote.eql? "Yes"
      @proposal.total_yes += 1
      @proposal.save
    end

    if @vote.eql? "No"
      @proposal.total_no += 1
      @proposal.save
    end

    if @vote.eql? "Maybe"
      @proposal.total_maybe += 1
      @proposal.save
    end

    @voter = @proposal.voters.create

    permitted_params = params.permit(:vote_email, :vote_name)
    # todo not sure why we are filtering with params.permit, since we access them by the same key below

    @voter.email = permitted_params[:vote_email]
    @voter.name = permitted_params[:vote_name]
    @voter.vote = @vote.downcase

    if @voter.save

      # send an email confirming the proposal was created
      NewVoteMailer.vote_email(@proposal).deliver

    end
  # todo handle error on save

  end

  def show
    @proposal = Proposal.find_by(proposal_id: params[:proposal_id])
  end

end
