class ProposalMailer < ActionMailer::Base
  default from: "shawnbutton@gmail.com"

  def proposal_email(proposal)
      @proposal = proposal

      mail(to: @proposal.email, subject: 'Your New Proposal Has Been Created:' + @proposal.title)
  end


end
