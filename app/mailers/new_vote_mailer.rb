class NewVoteMailer < ActionMailer::Base
  default from: "shawnbutton@gmail.com"

  def vote_email(proposal)
      @proposal = proposal

      mail(to: @proposal.email, subject: 'A new vote has been registered for your proposal:' + @proposal.title)
  end


end
