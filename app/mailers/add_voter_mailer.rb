class AddVoterMailer < ActionMailer::Base
  default from: 'shawnbutton@gmail.com'

  def add_voter_email(voter, proposal)
      @voter = voter
      @proposal = proposal

      mail(to: @voter.email, subject: 'You have been invited to vote in a new proposal:' + @proposal.title)
  end
end
