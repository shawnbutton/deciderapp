Rails.application.routes.draw do

  get 'my_proposals/view'

  devise_for :users
  get 'stats/view/:proposal_id' => 'stats#view', as: :stats_view

  get 'welcome/index'

  post 'proposals/email'

  #resources :proposals
  get '/proposals/new'
  post '/proposals/new' => 'proposals#create'
  post '/proposals' => 'proposals#create'
  get '/proposals/:proposal_id' => 'proposals#show', as: :show_proposal

  get '/proposals/:id/edit(.:format)' => 'proposals#edit', as: :edit_proposal

  delete '/proposals/:id(.:format)' => 'proposals#destroy', as: :delete_proposal

  # resources :proposals, only: [:new, :create, :show, :edit, :destroy]

  post 'vote/vote'
  get '/vote/show/:proposal_id' => 'vote#show', as: :vote



  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
